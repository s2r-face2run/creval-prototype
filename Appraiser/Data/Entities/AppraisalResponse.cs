﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Data.Entities
{
    public class AppraisalResponse
    {
        [Key]
        public int Id { get; set; }

        public int AppraisalRequestId { get; set; }

        [ForeignKey("AppraisalRequestId")]
        public AppraisalRequest AppraisalRequest {get; set;}

        public int ValuationId { get; set; }
        public decimal ValuationAmount { get; set; }
        public int ReliabilityScore { get; set; }

        [MaxLength(45)]
        public string RecommendationText { get; set; }

        [MaxLength(45)]
        public string CredConfInterval { get; set; }

        [Required]
        public DateTime ValuationDate { get; set; }

        [Required]
        [MaxLength(10)]
        public string IsLevel1Sufficient { get; set; }

        public int StatusCode { get; set; }
        public string Status { get; set; }
    }
}

﻿using Appraiser.Data.Models.Request;
using Appraiser.Data.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public interface IAppraiserService
    {
       public Task<AppraisalResponseDTO> RequestAppraisal(AppraisalRequestDTO request);
    }
}

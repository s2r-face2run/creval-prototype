﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updaterequestresponsemodellatlongnumbedroomssaledatevaldate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ValuationDate",
                table: "AppResponses",
                type: "datetime",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<decimal>(
                name: "BuildingSqFt",
                table: "AppRequests",
                type: "decimal(11, 2)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<decimal>(
                name: "Latitude",
                table: "AppRequests",
                type: "decimal(11, 8)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Longitude",
                table: "AppRequests",
                type: "decimal(11, 8)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "NumBedrooms",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "SaleDate",
                table: "AppRequests",
                type: "datetime",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValuationDate",
                table: "AppResponses");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumBedrooms",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "SaleDate",
                table: "AppRequests");

            migrationBuilder.AlterColumn<int>(
                name: "BuildingSqFt",
                table: "AppRequests",
                type: "int",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 2)");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class removeoldparkingfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumParkingSpacesByType",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "ParkingSpacesType",
                table: "AppRequests");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumParkingSpacesByType",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ParkingSpacesType",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");
        }
    }
}

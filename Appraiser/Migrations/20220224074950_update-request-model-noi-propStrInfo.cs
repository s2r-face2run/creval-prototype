﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updaterequestmodelnoipropStrInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GrossIncome",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropStrAddress",
                table: "AppRequests");

            migrationBuilder.AddColumn<string>(
                name: "PropStrName",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "NetOperatingIncome",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 2)");

            migrationBuilder.AddColumn<int>(
                name: "PropStrNumber",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PropStrNumber",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                 name: "PropStrName",
                 table: "AppRequests");

            migrationBuilder.AddColumn<string>(
                name: "PropStrAddress",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<decimal>(
                name: "NetOperatingIncome",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(10)",
                oldMaxLength: 10);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossIncome",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}

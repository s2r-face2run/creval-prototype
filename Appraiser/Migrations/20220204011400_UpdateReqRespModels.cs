﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class UpdateReqRespModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PropertyType",
                table: "AppRequests");

            migrationBuilder.AddColumn<string>(
                name: "AdjPropTypes",
                table: "AppRequests",
                type: "varchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "CurrentMonthRental",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "EmailId",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FamilyCharityRent",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "FamilyCharityRentCmnts",
                table: "AppRequests",
                type: "varchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NumParkingSpacesByType",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "OriginalFinancingAmount",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OriginalPurchasePrice",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ParcelNumber",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParkingSpacesType",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropAdditionalInfo",
                table: "AppRequests",
                type: "varchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PropCity",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropCounty",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropState",
                table: "AppRequests",
                type: "varchar(2)",
                maxLength: 2,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropStrAddress",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropSubCategory",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropSubCategoryOther",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PropType",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PropZipCode",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RemodelAddition",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RemodelAdditionYearRange",
                table: "AppRequests",
                type: "varchar(15)",
                maxLength: 15,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RentLow",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RepairRemodel",
                table: "AppRequests",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RepairRemodelCmnts",
                table: "AppRequests",
                type: "varchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "AppRequests",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusCode",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalParkingSpaces",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "VacRateCmnts",
                table: "AppRequests",
                type: "varchar(45)",
                maxLength: 45,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "ValuationId",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "YearBuilt",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdjPropTypes",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "CurrentMonthRental",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "EmailId",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "FamilyCharityRent",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "FamilyCharityRentCmnts",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumParkingSpacesByType",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "OriginalFinancingAmount",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "OriginalPurchasePrice",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "ParcelNumber",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "ParkingSpacesType",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropAdditionalInfo",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropCity",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropCounty",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropState",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropStrAddress",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropSubCategory",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropSubCategoryOther",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropType",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "PropZipCode",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "RemodelAddition",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "RemodelAdditionYearRange",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "RentLow",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "RepairRemodel",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "RepairRemodelCmnts",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "StatusCode",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "TotalParkingSpaces",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "VacRateCmnts",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "ValuationId",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "YearBuilt",
                table: "AppRequests");

            migrationBuilder.AddColumn<string>(
                name: "PropertyType",
                table: "AppRequests",
                type: "text",
                nullable: false);
        }
    }
}

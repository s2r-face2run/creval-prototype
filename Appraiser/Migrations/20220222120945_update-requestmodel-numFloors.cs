﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updaterequestmodelnumFloors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumFloors",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumFloors",
                table: "AppRequests");
        }
    }
}

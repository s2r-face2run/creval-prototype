﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updatepropertydatarollback : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "downPayment",
                table: "PropertyData");

            migrationBuilder.DropColumn(
                name: "incGross",
                table: "PropertyData");

            migrationBuilder.DropColumn(
                name: "latitude",
                table: "PropertyData");

            migrationBuilder.DropColumn(
                name: "longitude",
                table: "PropertyData");

            migrationBuilder.DropColumn(
                name: "priceasking",
                table: "PropertyData");

            migrationBuilder.DropColumn(
                name: "salePriceComment",
                table: "PropertyData");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "downPayment",
                table: "PropertyData",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "incGross",
                table: "PropertyData",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "latitude",
                table: "PropertyData",
                type: "decimal(18, 2)",
                precision: 11,
                scale: 8,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "longitude",
                table: "PropertyData",
                type: "decimal(18, 2)",
                precision: 11,
                scale: 8,
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "priceasking",
                table: "PropertyData",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "salePriceComment",
                table: "PropertyData",
                type: "text",
                nullable: true);
        }
    }
}
